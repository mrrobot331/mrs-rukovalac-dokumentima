# MRS - Rukovalac dokumentima

Program omogućava upravljanje različitim dokumentima i kolekcijama dokumenata. To podrazumeva
deljenje, kopiranje, trajno čuvanje, brisanje i kreiranje dokumenta proizvoljne strukture.

# Startovanje programa

### Instalacija
Trenutno u razvoju.

### Pokretanje programskog proizvoda
U razvoju.

# Demo
Ovde će biti prikazan demo programskog proizvoda.

# Razvojno okruženje softvera i programski jezik

- [Visual Studio Code](https://code.visualstudio.com/) - ([Python](https://www.python.org/)) - Kompletna implementacija softvera uz podršku QT Python framework-a
- [Astah Professional](https://astah.net/products/astah-professional/) - Alat za modelovanje softvera

# Autori
- **Ivan Jelić**
- **Bogdan Kotarlić**
- **Marko Videkanić**
- **Srđan Horvat**



